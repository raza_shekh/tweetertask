</div>
        <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span><b>Copyright © Twetter Task <?= date('Y'); ?></b></span>
              <p></p>
              <p>Design &amp; Developed By <a href="https://stzsoft.com/">M. Raza Shekh</p>
            </div>
          </div>
        </footer>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
<!--     <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div> -->

    <!-- Bootstrap core JavaScript-->
    <script src="{{url('assets/js/jquery.min.js">')}}</script>
    <script src="{{url('assets/js/bootstrap.bundl')}}e.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{url('assets/js/jquery.easing.m')}}in.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{url('assets/js/sb-admin.min.js')}}"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-growl/1.0.0/jquery.bootstrap-growl.min.js"></script>

<!-- Bootstrap Growl -->
  </body>

</html>
