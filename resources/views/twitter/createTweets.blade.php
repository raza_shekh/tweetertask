@extends('layouts.layout')
    @section('myContent')
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Tweet From Here</div>

                        <div class="panel-body">
                                <form method="POST" action="{{ route('mytweet') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}


                                @if(count($errors))
                                    <div class="alert alert-danger">
                                        <strong>Whoops!</strong> There were some problems with your input.
                                        <br/>
                                        <ul>
                                            @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif


                                <div class="form-group">
                                    <label>Tweet Text:</label>
                                    <textarea class="form-control" name="tweet"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Upload Images:</label>
                                    <input type="file" name="images" class="form-control">
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-success">Tweet</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection