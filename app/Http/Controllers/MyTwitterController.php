<?php

namespace App\Http\Controllers;

use App\MyTwitter;
use Illuminate\Http\Request;
use Auth;

class MyTwitterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('twitter.createTweets');
    }

    public function tweet(Request $request)
    {
        $this->validate($request,[

            'tweet' => 'required'
        ]);

        $myTwit = ['status' => $request->tweet];
        
        // if(!empty($request->images)){
        //     $media = $request->images[0]
        //         $myMedia = Twitter::uploadMedia(['media' => File::get($media->getRealPath())]);
        //         if(!empty($myMedia)){
        //             $newTwitte['media_ids'][$myMedia->media_id_string] = $myMedia->media_id_string;
        //         }
        // }

        $myTwits = Twitter::postTweet($myTwit);

        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MyTwitter  $myTwitter
     * @return \Illuminate\Http\Response
     */
    public function show(MyTwitter $myTwitter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MyTwitter  $myTwitter
     * @return \Illuminate\Http\Response
     */
    public function edit(MyTwitter $myTwitter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MyTwitter  $myTwitter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MyTwitter $myTwitter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MyTwitter  $myTwitter
     * @return \Illuminate\Http\Response
     */
    public function destroy(MyTwitter $myTwitter)
    {
        //
    }
}
