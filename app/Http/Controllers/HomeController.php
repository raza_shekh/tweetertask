<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         // Role::create(['name'=>'city']);
        //Permission::create(['name'=>'write']);
        //$permission = Permission::findById(1);
        // print_r($permission);exit();
        //$role       = Role::findById(1);
        //$role->givePermissionTo($permission);
        
       // auth()->user()->givePermissionTo('edit post');
       // auth()->user()->assignRole('writer');
        
        //return auth()->user()->getAllPermissions();
        //auth()->user()->assignRole('writer');
        return User::Permission('write')->get();
        //return view('home');
    }
}
